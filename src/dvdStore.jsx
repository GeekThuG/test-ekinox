import React, { useState } from "react";

const DVDStore = () => {
  const [basket, setBasket] = useState("");
  const [totalPrice, setTotalPrice] = useState(0);

  const prices = {
    "Back to the Future 1": 15,
    "Back to the Future 2": 15,
    "Back to the Future 3": 15,
    "Other Movie": 20,
  };

  const calculateTotal = (basketItems) => {
    const items = basketItems.trim().split("\n");
    const itemCounts = {};

    items.forEach((item) => {
      if (item in prices) {
        itemCounts[item] = (itemCounts[item] || 0) + 1;
      }
    });

    let total = 0;

    for (const item in itemCounts) {
      total += prices[item] * itemCounts[item];
    }

    if (
      itemCounts["Back to the Future 1"] ||
      itemCounts["Back to the Future 2"] ||
      itemCounts["Back to the Future 3"] ||
      itemCounts["Other Movie"]
    ) {
      if (Object.keys(itemCounts).length === 3) {
        total *= 0.8;
      } else if (Object.keys(itemCounts).length === 2) {
        total *= 0.9
      } else if (Object.keys(itemCounts).length > 3) {
        total *= 0.8;
        total += (Object.keys(itemCounts).length - 3) * prices["Other Movie"];
      }
    }

    return total;
  };

  const handleBasketChange = (event) => {
    setBasket(event.target.value);
    setTotalPrice(calculateTotal(event.target.value));
  };

  return (
    <div>
      <h1>DVD Store</h1>
      <textarea
        rows="5"
        cols="40"
        placeholder="Enter your DVDs items"
        value={basket}
        onChange={handleBasketChange}
      />
      <div>
        <p>Total Price: {totalPrice} €</p>
      </div>
    </div>
  );
};

export default DVDStore;
